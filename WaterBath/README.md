<img src="../Photos/waterbath.jpg" width="100%" align="center">





## Index



[TOC]



## Introduction

The idea of this equipment is to be an affordable and easily hackable alternative to incubate and visualize LAMP amplification. The total production price is intended to be around **5€**.

It serves as a waterbath for incubating the reactions at constant 63ºC (The temperature can be changed with a resistor in the electronics) with an integrated transilluminator made out of 470nm LEDs that light the chamber from the side together with a top orange filter that only let's go through the green fluorescence of the reactions.

For maintaining the simplicity and the low price of the system all the electronics are analogic, except the USB-C power deliver negotiator, which allows the system to be supplied by a type C charger with 9V|12V or a power bank.

<div align="center">
<img src="../Photos/waterbath_exploded.PNG" width="100%" align="center">
</div>



## Usage

- Choosing the right charger. Voltage switching between 9V and 12V. 
- If you are working with 9V we recommend boiling the water and leaving it at room temperature for 5-10 minutes to accelerate the heating process.

## Results

<div align="center">
<img src="../Photos/waterbath_results.png" width="100%" align="center">
</div>

## How to make it / Parts

All the 3D models for the different parts can be obtained from the [Onshape link](https://cad.onshape.com/documents/2b8d83c057de2222e1a47998/w/63dbac67563c7d0733092280/e/a6035fbb4205682301a51b0d?renderMode=0&uiState=61df135f8cc99e41b23786a9). 


**Why this software?**

>We decided to use Onshape because it's free, allows a GitHub-like edition and it's powerful as other commercial software. Files can also be exported in numerous formats to be used by Open Source software like [freecad](https://www.freecadweb.org/).

All the gerbers, schematics, and board files from the electronic parts can be found in the [easyEDA link](https://oshwlab.com/franxi2953/simple-water-bath).

**Why this software?**

>We decided to use easyEDA because it's also free and allows users to branch and create their versions of existing projects.
>
>Talking about its features, easyEDA is software in between an industrial PCB designing software (With production features and the manufacturers' list of parts at the assembly line), and a hobbyist one (Simple and intuitive).
>
>Additionally, their community Oshwlab (Open Source HardWare Lab) eases the sharing of the designs.

If you want to create your own version of any of the parts, please, contact us to include you as an Onshape editor. We will be more than happy to see new versions of this device.

### Main PCB

#### Before starting

All the components can be already soldered using the [JLCPCB](https://jlcpcb.com/SMT?gclid=Cj0KCQiAosmPBhCPARIsAHOen-OMsx8HIXeYEDgWpx64psOBugTITP5Y58o4RbJouNwXl_FnbqOctwsaAkR5EALw_wcB) SMT assembly line. All the parts elected belong to their catalog except for the USB-C controller, the  [IP2721_MAX12](https://datasheet.lcsc.com/szlcsc/2006111335_INJOINIC-IP2721_C603176.pdf). You can buy it from [Aliexpress](https://es.aliexpress.com/item/1005002316588641.html) (**Take care**, the MAX12 prefix is important!) and solder it separately.



#### How to build the electronics

<div align="center">
<img src="../Photos/waterbath_instructions.png" width="100%" align="center">
</div>


**The photo above is the first version of the machine, the new one comes with an LED to indicate when the heater is working and a potentiometer. Its use is better explained in the next section.**


For making it possible to solder all the components at JLCPCB, the PCB is divided into two sides; The LEDs, which need to be facing the interior of the WaterBath, and the rest of the electronics, which needs to be facing the outside where the USB-C charger is plugged.



Both PCBs come connected together.  For building the machine you need to cut them apart, polish the borders and the pins in the bottom side of the PCB, and then solder both of them joining their backs and taking care that the "VCC" and "Resistors" labels are aligned.

#### Schematic explained

<div align="center">
<img src="../Photos/waterbath_schematic.png" width="100%" align="center">
</div>
**USB-C Sink Chip**

This is the circuit controlling the chip IP2721_MAX12. In the USB-C standard, different ranges of voltages can be negotiated between the device and the power supply. That's the main job of the IP2721_MAX12.



[Despite all the problems that this chip can originate](https://hackaday.io/project/172187-ts100-flex-c-friend/log/179029-ip2721-version-built-but), we have chosen it because of the price and the simplicity of its use, but mainly because it doesn't require to be programmed through I2C for selecting the final voltage. It can just be adjusted with resistors in the "SEL" pin.



The switch SW1 allows the user to select between two modes, 9V or 12V, depending on the charger available. **Remember that a USB-C charger that can supply 3A at the selected voltage is required**, otherwise, the system will only work at 5V which is insufficient for the heating and lighting.



**LED control**

This circuit is a basic LED controller circuit that regulates 30 mA through the LEDs at 12V. The switch helps the user to activate and deactivate the circuit when it's needed.



**Temperature control**

We designed an analogic temperature control circuit based on the 555 chip. For more information about how the system works, we recommend taking a look at external [links](https://www.electroschematics.com/555-temperature-controller-circuit/). 



In a nutshell; We chose a standard 10k NTC thermistor. The machine is designed in a way that at 63ºC, the resistance on R13 + the thermistor one is equal to two times R11 (2.4k). For choosing the proper values of this resistance we immerse the thermistor in a water bath at 63ºC and measure its resistance. As different thermistors from the same manufacturer have a 5% range of variation, a first run must be tried to adjust the R13 potentiometer to the best value that makes the system stop at exactly 63ºC (Try it experimentally). Different values for the temperature can be obtained by changing the R11 resistor.  A possibility for a future maker will be to create different versions of this system that incubates at a different temperature or even that allow the user to choose.



The reason to design an analogic control circuit is to pursue simplicity and robustness, erasing the need for any code to rule the waterbath. An additional advantage is the affordability of the 555 chip together with the fact that it's available in almost any SMT assembly line.

### Heater

<div align="center">
<img src="../Photos/heater.jpg" width="80%" align="center">
</div>


After trying different heaters at Aliexpress we decided to build our own PCB heater. Small-scale aluminum PCB manufacturing can reach prices like 0.2$ per unit and allows you to produce your own designs. 

For the trace width calculation we used the following formula:

```math
R = \rho \cdot \frac{L}{T \cdot W} \cdot [1 + \alpha (temp -25)]
```
<div align="center" style="font-size:8">
<b>Where</b>: R = resistance, ρ = resistivity, L = length, T = trace height, W = trace width, α = temperature coefficient, temp = temperature </center>
</div>
<br/><br/>

We calculate the trace to consume around 1.5 A at 12V and 60ºC. The resulting resistance is 8Ohm and the length is 217cm and the width 0.16mm.

For avoiding electrolysis and copper oxidation we covered the top side of the PCB with a urethane spray.

### Case

We have chosen the case material with two main characteristics in main; the water tightness and the material resistance to heat deformations. We mainly experimented with two materials:

- **Resin (SLA printing):** Although its price resin has a really nice heat resistance and is completely waterproof. For the first trials, we used a [Formlabs](https://formlabs.com/3d-printers/form-3/) system together with their commercial Gray resin. However, even if the machine is powerful and easy to use, the system is expensive and the material required (~60mL) cost around 15€. For more affordable printing other brands can be used (Such as [Creality](https://www.creality.com/product/resin-3d-printer) or [Anycubic](https://www.anycubic.com/products/anycubic-photon-3d-printer)) with resin prices more than 3 times lower.



- **PETG (FDM printing):** As FDM 3D printers are much more extended in the maker labs around the world we explored PETG as a solution for printing the case. Is important to note that the long-established materials for FDM printing (ABS/PLA) will experience _warping_ at the 63ºC that the reaction requires. PETG is not only more heat resistant but also chemically inherited, making it the gold standard 3D printing material for biotechnology. The price of 1Kg of PETG cost around 20€, making the case expense a few euros. **Help needed!** The 3D printed model needs to be optimized for FDM.

**Warning!** 

> The little windows on the case, have 0.2mm of thickness and are prone to break. If that happens the water will leak damaging the electronics. To avoid this, after the case is printed, pour a bit of silicone/PDMS in the inside of each window (The side where the LEDs enter) to make them water tight.



### Optics

<div align="center">
<img src="../Photos/wavelengths.png" width="100%" align="center">

</div>

As it can be seen in the schematic at the [easyEDA](https://oshwlab.com/franxi2953/simple-water-bath) link, the LED used is the [67-21SUBC/S400-XX/TR8](https://www.mouser.es/datasheet/2/143/ever_s_a0007426159_1-2267186.pdf) from Everlight. We have chosen it regarding the peak wavelength of 468 nm (near the Fluorescein excitation peak) and the availability for SMT assembly by JLCPCB.

For filtering the emission wavelength of the LED and removing it from the fluorescence we tried the following solutions:
- **[Lee filter 105](https://www.leefilters.com/lighting/colour-details.html#105&filter=cf) mounted in a laser-cut acrylic:** Lee filters are a really affordable supplier of light filters. They have a broad range of really well-characterized filters to choose from. The filter can be mounted in the 3D printed or laser cut version of the holder model downloaded from [Onshape](https://cad.onshape.com/documents/2b8d83c057de2222e1a47998/w/63dbac67563c7d0733092280/e/a6035fbb4205682301a51b0d?renderMode=0&uiState=61df135f8cc99e41b23786a9). The fluorescence can be read better by putting together two layers of the filter.

<div align="center">
<img src="../Photos/filter.jpg" width="80%" align="center">



</div>

<br/><br/>



### Biotechnology

Initially, this hardware has been used for detecting SARS-CoV-2 applying the [CoronaDetective protocol](https://www.protocols.io/view/corona-detective-user-protocol-v2-0-bpwzmpf6), but any LAMP protocol can be tried. In a nutshell, the CoronaDetective reactions can be prepared using the following materials:

| Reagent                                                      | Concentration / Units |
| ------------------------------------------------------------ | --------------------- |
| [dNTPs](https://international.neb.com/products/n0447-deoxynucleotide-dntp-solution-mix#Product%20Information) | 1.4 mM                |
| [NEB isothermal amplification buffer](https://international.neb.com/products/b0537-isothermal-amplification-buffer#Product%20Information) | 1X                    |
| Additional MgSO4 <br />(The NEB buffer already have 2mM)     | 5mM                   |
| Primer Mix                                                   | 1X                    |
| [RTx](https://international.neb.com/products/m0380-warmstart-rtx-reverse-transcriptase#Product%20Information) | 7.5 Units             |
| [Bst 2.0](https://international.neb.com/products/m0537-bst-20-dna-polymerase#Product%20Information) | 8 Units               |
| Syto82                                                       | 1X                    |
| Sample<br />(We used [BEI inactivate Virus](https://www.beiresources.org/Catalog/antigen/NR-52286.aspx)) | 20-200 Units          |



The PrimerMix at 10X concentration can be prepared with the following sequences:

 

| Primer name | Primer sequence                               | Concentration <br />(for a 10X stock) |
| ----------- | --------------------------------------------- | ------------------------------------- |
| F3          | AACACAAGCTTTCGGCAG                            | 2uM                                   |
| B3          | GAAATTTGGATCTTTGTCATCC                        | 2uM                                   |
| FIP-FAM     | FAM-TGCGGCCAATGTTTGTAATCAGCCAAGGAAATTTTGGGGAC | 16uM                                  |
| FIP-Q       | CATTGGCCGCA-Q                                 | 24uM                                  |
| BIP         | CGCATTGGCATGGAAGTCACTTTGATGGCACCTGTGTAG       | 16uM                                  |
| LF          | TTCCTTGTCTGATTAGTTC                           | 8uM                                   |
| LB          | ACCTTCGGGAACGTGGTT                            | 8uM                                   |



## How to help / Join the project

- [ ] Take a look at the [electronics schematics](). Hack it, branch it and create your own versions.
- [ ] Help us to adapt the hardware to other formats! (for example; for 96 well plates)
- [ ] Contact us! We are happy to enlarge the community around these devices and brainstorm together.