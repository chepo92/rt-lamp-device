

<div align="center">
    <img src="Photos/qLAMP.jpg" width="90%" align="center">
</div>

# Index

[TOC]

# Introduction (Help needed!)

This machine is 🚧 On construction 🚧, which means that we are still testing and changing the modules. If you want to join the effort of designing this first prototype, we encourage you to contact us. Don't expect the hardware to work perfectly.



In each section, you can check the To-Do list for each module.

# Components

## Electronics

Find them at [Open Source Hardware Lab](https://oshwlab.com/franxi2953/rt-lamp-bottom).

### To-Do list:

#### General

- [ ] Connect level shifters at VCC.
- [ ] PCB bigger, the ESP32 should not go out from it.
- [ ] Include a USB-C PD sink chip, so everything go connected and powered through USB-C.

#### Temperature module

- [x] ESP32 reading of the 3 thermistors.
- [x] ESP32 control of the temperature through a PID.
- [ ] Final calibration with the final case.

#### Wifi server

- [x] mDNS working at qLAMP.local
- [x] Webpage in the data folder uploaded with ESP32 Filesystem Uploader.
- [x] Webpage served by ESP32 working with HTML/CSS/JS that perform GET requests to the ESP32 to control it.
- [x] Control of temperature implemented. Visualization with [Chart.js](https://www.chartjs.org/)
- [ ] Fluorescence reading.

#### Light source module

- [x] ESP32 communicating successfully with the WS2814
- [ ] Find a library that substitutes the FastLED one and allows control of the white channel (The fourth LED).

#### Analog reading module

- [ ] ESP32 communicates with the ADC through I2C.
- [ ] Find the correct closed-loop resistors for detecting lamp fluoresce.
- [ ] The module can follow an entire amplification.

## Metal parts

We used metal 3D printing for the [tube holder](https://cad.onshape.com/documents/fce8700b3c8f4038beef7327/w/83d45921ebc4fc25dd1359ba/e/2e0bbe7309544f9a3216837c?renderMode=0&uiState=61f4408d74beb127bedc0f6d).

We recommend [Shenzhen companies](https://i.facfox.com/) as they produce well-finished parts at an amazing cost (starting at ~15€/part).

## Optics

- [ ] Design a way to filter the source light from the LEDs.

## Case

🚧 On construction 🚧

# Software

## ESP32 code

To program the ESP32, [this tool](https://randomnerdtutorials.com/install-esp32-filesystem-uploader-arduino-ide/) is needed (Install the support for the esp32 in the Arduino IDE and the Filesystem Uploader Plugin). It's used for uploading the control webpage to the ESP32 flash memory.

As we are prototyping, the code may not be clear for someone new. Please do not hesitate in contacting us for a better explanation.