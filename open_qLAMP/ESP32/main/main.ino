#include <WiFi.h>   
#include <WebServer.h>
#include <SPIFFS.h> 
#include <ESPmDNS.h>
#include "src/analogWrite.h"

#include <PID_v2.h>

const char* ssid = "SFR_02B0";  // Replace by your wifi
const char* password = "zfi54nbir5gad49erez9";   // Replace by your wifi

WebServer server(80);

#define HEATER_PIN 18
int temp_pin[3] = {32,33,34};
int mean_temp = 0;

float heater_goal = 0;

double Kp = 50, Ki = 5, Kd = 5;
PID_v2 myPID(Kp, Ki, Kd, PID::Direct);

void setup() {
  Initialize();
}

void loop() {
  server.handleClient();

  runPID();

}
