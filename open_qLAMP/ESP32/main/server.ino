//Initialization

void Initialize() {
  Serial.begin(9600);
  delay(1000);

  if(!SPIFFS.begin()){
    Serial.println("An Error has occurred while mounting SPIFFS");
    return;
  }

  if(!MDNS.begin("qLAMP")) {
     Serial.println("Error starting mDNS");
     return;
  }

  serverBegin();

  pinMode(HEATER_PIN, OUTPUT);

  
  myPID.Start(calculate_temperature(),0,0);
}


//SERVER FUNCTIONS
void handleNotFound() {
  String message = "File Not Found\n\n";
  message += "URI: ";
  message += server.uri();
  message += "\nMethod: ";
  message += (server.method() == HTTP_GET) ? "GET" : "POST";
  message += "\nArguments: ";
  message += server.args();
  message += "\n";
 
  for (uint8_t i = 0; i < server.args(); i++) {
    message += " " + server.argName(i) + ": " + server.arg(i) + "\n";
  }
 
  server.send(404, "text/plain", message);
}

bool loadFromSPIFFS(String path) {
  String dataType = "text/html";
 
  Serial.print("Requested page -> ");
  Serial.println(path);
  if (SPIFFS.exists(path)){
      File dataFile = SPIFFS.open(path, "r");
      if (!dataFile) {
          handleNotFound();
          return false;
      }
 
      if (server.streamFile(dataFile, dataType) != dataFile.size()) {
        Serial.println("Sent less data than expected!");
      }else{
        Serial.println("Page served!");
      }
 
      dataFile.close();
  }else{
      handleNotFound();
      return false;
  }
  return true;
}

void serverBegin () {
  Serial.println("\n");
  WiFi.begin(ssid, password);
  Serial.print("Connecting to WiFi ...");

  while (WiFi.status() != WL_CONNECTED)
  {
    Serial.print(".");
    delay(100);
  }

  Serial.println("\n");  
  Serial.println("Connected !");
  Serial.print("IP: ");
  Serial.println(WiFi.localIP());

  server.on("/", []() {loadFromSPIFFS("/index.html");}); 
  server.on("/ClickHeater", ClickHeater);
  server.on("/temp", SendTemp);     
  server.serveStatic("/",SPIFFS,"/");

  
  server.onNotFound(handleNotFound);  
  server.begin();  
  Serial.println("Server online.");
}



//TEMPERATURE FUNCTIONS
void ClickHeater(){
  if (!server.arg("degrees"))
  {
    server.send(200, "text/plain", "[ERROR] TARGET TEMPERATURE REQUIRED");
  } else {
    float deg = String(server.arg("degrees")).toFloat();
    if (deg < 0 || deg>100)
    {
      server.send(200, "text/plain", "[ERROR] TEMPERATURE NOT IN RANGE [0ºC-100ºC]");
    } else {
    server.send(200, "text/plain", "TARGET TEMPERATURE SET TO " + String(deg));
    heater_goal = deg;
    Serial.println("Heater new target: " + String(heater_goal) + "ºC");
    
    }
  
    myPID.Start(calculate_temperature(),0,heater_goal);
 
  }
  
}

void SendTemp()
{
  server.send(200, "text/plain", String(calculate_temperature(),4));
}

float calculate_temperature ()
{
  //////Calculate temperature//////
  
  //Read ADC and transform into voltage
  float voltage[3];
  voltage[0] = analogRead(temp_pin[0]) * (3.300000 /*volts*/ / 4096 /*max adc value*/);

  //Voltage to resistance calculation
  float resistance[3];

  resistance[0] = (20/*K Resistor in series*/ * voltage[0]) / (11.75 /*Vcc*/ - voltage[0]);
  //Serial.println(resistance[0],4);

  //Resistance to temperature
  float temperature[3];
  //https://www.thinksrs.com/downloads/programs/Therm%20Calc/NTCCalibrator/NTCcalculator.htm
  float c1 = -1.554231789e-03;
  float c2 = 6.590828007e-04;
  float c3 = -14.23606585e-07;
  float LogR = log(resistance[0]*1000);
  temperature[0] = (1.0 / (c1 + c2*LogR + c3*LogR*LogR*LogR))-273.15;
  
  return temperature[0];

  //Doing the mean
}

int runPID() {
  int temp = calculate_temperature();
  int PID_data = myPID.Run(temp);
  //Serial.println(String(PID_data) + " " + String(heater_goal));
  analogWrite(HEATER_PIN, PID_data/2);
}
