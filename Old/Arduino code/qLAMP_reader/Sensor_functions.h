#ifndef qLAMP_h
#define qLAMP_h

#include "Arduino.h"
#include <Adafruit_ADS1015.h>
#include <PID_v2.h>

#define MAX_ADC_VALUE 65535
#define AUX_RESISTOR 8.2

#define PWM_pin 11
#define DA 2
#define DB 3
#define DC 4
#define INH 5

class io_status {
  private:
  
  //input
    float temperature[3];
    float PD[8];
    Adafruit_ADS1115 temperature_array {0x4B};
    Adafruit_ADS1115 PD_array_1 {0x48};
    Adafruit_ADS1115 PD_array_2 {0x49};
    
    double Kp = 50, Ki = 5, Kd = 5;
    PID_v2 t_PID{Kp, Ki, Kd, PID::Direct};
    int target_temp;

    float read_temperature(int sensor_number);
  
  
  //output
    int PWM_power;
    byte LED;
    

  public:
    io_status();
    int update_temperature();
    int send_data();
    int set_temperature(int temp);
    int run_PID();
    int led_on (int led);
    int read_fluorescence(int channel);
    int manage_serial();
    void commands_info();

    
};

#endif
