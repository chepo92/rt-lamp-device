#include <Wire.h>
#include "Sensor_functions.h"

io_status sensors;

bool PID_time = false;

void setup() {
  Serial.begin(9600);

  sensors.commands_info();
  Serial.flush();
  
}

void loop() {
  
  sensors.manage_serial();

  sensors.run_PID();
}
