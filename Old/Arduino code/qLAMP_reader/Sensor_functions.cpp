#include "Arduino.h"
#include "Sensor_functions.h"
#include <math.h>
#include <ArduinoJson.h>
#include <Adafruit_ADS1015.h>
#include <PID_v2.h>


io_status::io_status()  {
  temperature_array.setGain(GAIN_TWOTHIRDS);  // 2/3x gain +/- 6.144V  1 bit = 3mV      0.1875mV (default)
  PD_array_1.setGain(GAIN_SIXTEEN);  // 2/3x gain +/- 6.144V  1 bit = 3mV      0.1875mV (default)
  PD_array_2.setGain(GAIN_SIXTEEN);  
  
  temperature_array.begin();
  PD_array_1.begin();
  PD_array_2.begin();
  
  led_on(0);

  pinMode(DC,OUTPUT);
  pinMode(DB,OUTPUT);
  pinMode(DA,OUTPUT);
  pinMode(INH,OUTPUT);
  
}

float io_status::read_temperature (int sensor_number){
  
  int TH, TH_REF;
  float TH_voltage, TH_voltage_reference, TH_resistance;

  if (sensor_number < 0 && sensor_number > 3)
    return -1;
  
  TH = temperature_array.readADC_SingleEnded(sensor_number);
  TH_REF = temperature_array.readADC_SingleEnded(3);
  
  if (TH >= MAX_ADC_VALUE || TH < 0)
    return -2;

  TH_voltage = 0.0001875 * TH;
  TH_voltage_reference = 0.0001875 * TH_REF;

  TH_resistance = ((2 * AUX_RESISTOR * TH_voltage_reference) / TH_voltage) - AUX_RESISTOR; 

//  DEBUG
//  ----------------------------------------------------
//  Serial.print(TH);
//  Serial.print(",");
//  Serial.println(TH_REF);
//  Serial.print(TH_voltage, 8);
//  Serial.print(",");
//  Serial.print(TH_voltage_reference, 8);
//  Serial.print(",");
//  Serial.print(TH_voltage_reference/TH_voltage, 8);
//  Serial.print(",");
//  Serial.print(TH_resistance, 4);
//  Serial.print(",");
//  -----------------------------------------------------
  
//Function obtained by exponential regression of a NTC 10k 3095 temptable 
  temperature[sensor_number] = log((TH_resistance - 0.5261)/31.4839)/-0.04758;
  return 1;
}

int io_status::update_temperature () {
  int error;
  
  for (int i  = 0; i < 3; i++)
  {
     error = read_temperature(i);
     if (error != 1)
      return error; 
  }
}

int io_status::send_data()
{
  StaticJsonDocument<128> jsonized_variables; //https://arduinojson.org/v6/assistant/

  JsonArray temp = jsonized_variables.createNestedArray("temp");
  for (int i = 0; i < 3; i++)
    temp.add(temperature[i]);
    
  JsonArray PhotoDiodes = jsonized_variables.createNestedArray("PD");
  for (int i = 0; i <8; i++)
    PhotoDiodes.add(PD[i]);
   
  jsonized_variables["PWM"] = PWM_power;
  jsonized_variables["LED"] = LED;

  serializeJsonPretty(jsonized_variables, Serial);
  
  return 1;
}

int io_status::run_PID()
{
  int output;
  int error = update_temperature();

  //DEBUG
//  Serial.print(target_temp);
//  Serial.print(",");
//  Serial.print(temperature[0]);
//  Serial.print(",");
//  Serial.print(temperature[1]);
//  Serial.print(",");
//  Serial.println(temperature[2]);
  
  if (error != 1)
   {
    Serial.println("Error: Impossible to read temperature sensors");
    return error;
   }

  output = t_PID.Run((temperature[0] + temperature [1] + temperature [2]) / 3);
  analogWrite(PWM_pin, output);

  return 1;
}

int io_status::set_temperature(int temp)
{
  int output;
  int error = update_temperature();

  target_temp = temp;


  //DEBUG
  Serial.print(target_temp);
  Serial.print(",");
  Serial.print(temperature[0]);
  Serial.print(",");
  Serial.print(temperature[1]);
  Serial.print(",");
  Serial.println(temperature[2]);


  //delay(1000);
  
  if (error != 1)
   {
    return error;
   }
  
  t_PID.Start((temperature[0] + temperature [1] + temperature [2]) / 3, PWM_power,target_temp);

 

}

int io_status::led_on (int led)
{
  bitSet(LED, led);
  switch (led) {
  case 0:
    digitalWrite(DC, LOW);
    digitalWrite(DB, LOW);
    digitalWrite(DA, LOW);
    digitalWrite(INH, HIGH);
    break;
  case 1:
    digitalWrite(DC, LOW);
    digitalWrite(DB, LOW);
    digitalWrite(DA, LOW);
    digitalWrite(INH, LOW);
    break;
  case 2:
    digitalWrite(DC, LOW);
    digitalWrite(DB, LOW);
    digitalWrite(DA, HIGH);
    digitalWrite(INH, LOW);
    break;
  case 3:
    digitalWrite(DC, LOW);
    digitalWrite(DB, HIGH);
    digitalWrite(DA, LOW);
    digitalWrite(INH, LOW);
    break;
  case 4:
    digitalWrite(DC, LOW);
    digitalWrite(DB, HIGH);
    digitalWrite(DA, HIGH);
    digitalWrite(INH, LOW);
    break;
  case 5:
    digitalWrite(DC, HIGH);
    digitalWrite(DB, LOW);
    digitalWrite(DA, LOW);
    digitalWrite(INH, LOW);
    break;
  case 6:
    digitalWrite(DC, HIGH);
    digitalWrite(DB, LOW);
    digitalWrite(DA, HIGH);
    digitalWrite(INH, LOW);
    break;
  case 7:
    digitalWrite(DC, HIGH);
    digitalWrite(DB, HIGH);
    digitalWrite(DA, LOW);
    digitalWrite(INH, LOW);
    break;
   case 8:
    digitalWrite(DC, HIGH);
    digitalWrite(DB, HIGH);
    digitalWrite(DA, HIGH);
    digitalWrite(INH, LOW);
    break;
  default:
    break;  
  } 
  return 1;
}

int io_status::read_fluorescence(int channel)
{
  if (channel > 0 && channel < 5)
  {
    Serial.print(String(PD_array_1.readADC_SingleEnded(channel - 1)) + ","); //Careful LED 1 is the first LED but ADC reading 1 is the second (The first ADC reading it's actually the 0)
    delay(500);
    this->led_on(channel);  
    delay(500);
    Serial.println(PD_array_1.readADC_SingleEnded(channel - 1));
    delay(500);
    this->led_on(0);
    return 1;
  } else if (channel > 4 && channel < 9) {
    Serial.print(String(PD_array_2.readADC_SingleEnded(channel - 5)) + ",");
    delay(500);
    this->led_on(channel);  
    delay(500);
    Serial.println(PD_array_2.readADC_SingleEnded(channel - 5));
    delay(500);
    this->led_on(0);
    return 1;
  } else {
    Serial.println("[ERROR] Invalid channel number.");
  }
  
}

int io_status::manage_serial()
{
  if (Serial.available())
  {
    char buf = Serial.read();
    switch (buf) {
    case '0':
      this->commands_info();
      break;
    case '1':
     Serial.println("\nSending data..:\n");
     this->update_temperature();
     this->send_data();
     break;
    case '2':
    {
     String t_temp = Serial.readString();
     Serial.println("\nTarget temperature set to " + t_temp + "degrees Celsius.");
     this->set_temperature(t_temp.toInt());
     break;
    }
    case '3':
    {
     String led_number = Serial.readString();
     Serial.println("\nLed " + String(led_number.toInt()) + " on.");
     this->led_on(led_number.toInt());
     break;
    }
    case '4':
    {
     String channel_number = Serial.readString();
     Serial.println("\nFluorescence measurements in channel " + channel_number + ".");
     this->read_fluorescence(channel_number.toInt());
     break;
    }
    default:
     break;
   }

   while (Serial.available())
    Serial.read();
  }
  return 1;
}

void io_status::commands_info()
{
  Serial.println("Available commands");
  Serial.println("__________________\n");

  Serial.println("0 -> Commands available.");
  Serial.println("1 -> Send a json encoded output with all the sensors data.");
  Serial.println("2X -> Set target temperature to X.");
  Serial.println("3X -> Activate led X (Range [0:8] where 0 means all leds off).");
  Serial.println("4X -> Read fuorescence output on position X.");
}
